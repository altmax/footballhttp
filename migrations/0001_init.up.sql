-- ---
-- globals
-- ---

-- set sql_mode="no_auto_value_on_zero";
-- set foreign_key_checks=0;

-- ---
-- table 'gamer'
-- 
-- ---

-- drop table if exists gamer;
		
create table gamer (
  id integer not null auto_increment,
  exp integer not null default 0,
  money integer not null default 0,
  hardmoney integer not null default 0,
  wins integer not null default 0,
  loses integer not null default 0,
  misses integer not null default 0,
  name varchar(255) not null,
  goals integer not null default 0,
  unique_word varchar(255) not null,
  primary key (id),
  unique key (unique_word),
  unique key (name)
);

-- ---
-- table 'notification'
-- 
-- ---

-- drop table if exists notification;
		
create table notification (
  id integer not null auto_increment,
  gamer_id integer not null,
  text mediumtext not null,
  datetime integer not null,
  is_viewed tinyint(1) not null default 0,
  primary key (id),
  constraint foreign key (gamer_id) references gamer (id)
);

-- ---
-- table 'item'
-- 
-- ---

-- drop table if exists item;
		
create table item (
  id integer not null auto_increment,
  type integer not null default 1,
  set_id integer not null default 0,
  sprite varchar(255) not null,
  hair varchar(255) not null,
  foot varchar(255) not null,
  hide_hair tinyint(1) not null default 0,
  hide_hat tinyint(1) not null default 0,
  leg_visible tinyint(1) not null default 0,
  label varchar(255) not null,
  price varchar(10) not null default '0.0',
  bought tinyint(1) not null default 0,
  is_default tinyint(1) not null default 0,
  is_bonus tinyint(1) not null default 0,
  is_wearing tinyint(1) not null default 0,
  last_update integer not null,
  primary key (id)
);

-- ---
-- table 'inventory'
-- 
-- ---

-- drop table if exists inventory;
		
create table inventory (
  id integer not null auto_increment,
  item_id integer not null,
  gamer_id integer not null,
  on_body tinyint(1) not null default 0,
  primary key (id),
  constraint foreign key (gamer_id) references gamer (id),
  constraint foreign key (item_id) references item (id)
);



-- ---
-- table 'gamer_bonus'
-- 
-- ---

-- drop table if exists gamer_bonus;
		
-- create table gamer_bonus (
--   id integer not null auto_increment,
--   gamer_id integer not null,
--   bonus_id integer not null,
--   primary key (id),
--   constraint foreign key (gamer_id) references gamer (id)
-- );

-- ---
-- foreign keys 
-- ---

-- alter table notification add foreign key (gamer_id) references gamer (id);
-- alter table inventory add foreign key (item_id) references item (id);
-- alter table inventory add foreign key (gamer_id) references gamer (id);
-- alter table gamer_bonus add foreign key (gamer_id) references gamer (id);

-- ---
-- table properties
-- ---

alter table gamer engine=innodb default charset=utf8 collate=utf8_general_ci;
alter table notification engine=innodb default charset=utf8 collate=utf8_general_ci;
alter table inventory engine=innodb default charset=utf8 collate=utf8_general_ci;
alter table item engine=innodb default charset=utf8 collate=utf8_general_ci;
-- alter table gamer_bonus engine=innodb default charset=utf8 collate=utf8_general_ci;

-- ---
-- test data
-- ---

-- insert into gamer (id,exp,money,hardmoney,wins,loses,misses,name,goals,unique_word) values
-- ('','','','','','','','','','');
-- insert into notification (id,gamer_id,text,datetime,is_viewed) values
-- ('','','','','');
-- insert into inventory (id,item_id,gamer_id,on_body) values
-- ('','','','');
-- insert into item (id,type,set_id,sprite,hair,foot,hide_hair,hide_hat,leg_visible,label,price,bought,is_default,is_bonus,is_wearing,last_update) values
-- ('','','','','','','','','','','','','','','','');
-- insert into gamer_bonus (id,gamer_id,bonus_id) values
-- ('','','');