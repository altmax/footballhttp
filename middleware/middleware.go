package middleware

import (
	"net/http"

	"bitbucket.org/altmax/footballhttp/utils"
)

//CheckUniqKey проверка наличия поля id в request
func CheckUniqKey(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.NotFound(w, r)
	}
	if ok, _ := utils.CheckFormField(r, "id"); ok {
		http.NotFound(w, r)
	}
}

//ValidAPI check api key
func ValidAPI(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("X-API-KEY") != "Utqvlbpfqyth1" {
		w.WriteHeader(http.StatusUnauthorized)
	}
}
