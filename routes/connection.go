package routes

import "net/http"

//Connection возвращает данные игрока по id. Если такой игрок не существует, он будет создан
func Connection(r *http.Request) string {
	var gamer *Gamer
	id := r.FormValue("id")
	name := r.FormValue("name")
	gamer, ok := session.GetGamer(id)
	if !ok {
		gamer = session.Add(id, name)
	}
	return gamer.GetXMLString()
}
