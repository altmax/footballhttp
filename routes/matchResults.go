package routes

import (
	"net/http"
	"strconv"
)

//Update обновляет информацию о двух игроках после завершения матча
func Update(w http.ResponseWriter, r *http.Request) {
	data := GetFieldsFromForm(r, "id1", "id2", "exp1", "exp2", "win", "money1", "money2")
	if data == nil {
		http.NotFound(w, r)
	}
	gamer1, ok := session.GetGamer(data["id1"])
	if ok {
		exp1, _ := strconv.Atoi(data["exp1"])
		gamer1.Exp += exp1
		if data["win"] == data["id1"] {
			gamer1.Wins++
		}
	}
	gamer2, ok := session.GetGamer(data["id2"])
	if ok {
		exp2, _ := strconv.Atoi(data["exp2"])
		gamer2.Exp += exp2
		if data["win"] == data["id2"] {
			gamer2.Wins++
		}
	}
}
