package routes

import (
	"encoding/json"
	"encoding/xml"
	"io"
	"log"
	"net/http"

	"bitbucket.org/altmax/footballhttp/server"
	"bitbucket.org/altmax/footballhttp/utils"
)

//MatchMaking ищет комнату для игрока на мастер сервере. Возвращает токен комнаты и порт relay сервера
func MatchMaking(w http.ResponseWriter, r *http.Request) {
	data := utils.GetFieldsFromForm(r, "id")
	if data == nil {
		http.NotFound(w, r)
	}
	log.Println(data["id"])
	_, ok := session.GetGamer(data["id"])
	if !ok {
		http.NotFound(w, r)
	}
	resp, ok := server.sendMessage("/findRoom", data)
	// log.Println(resp)
	if ok {
		type Relay struct {
			Token string `json:"token" xml:"token,attr"`
			Port  string `json:"port" xml:"port,attr"`
		}
		var t Relay
		json.Unmarshal([]byte(resp), &t)
		// log.Println(t)
		if t.Token != "" {
			// log.Println("Token : ", t.Token)
			resp, ok = sendMessage("/getRelayPort", data)
			// log.Println(resp)
			if ok {
				json.Unmarshal([]byte(resp), &t)
				if t.Port != "" {
					b, _ := xml.Marshal(t)
					log.Println(string(b))
					io.WriteString(
						w,
						string(b),
					)
				}
			}
		}
	}
}
