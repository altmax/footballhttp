package models

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"io/ioutil"
	"os"
)

type Item struct {
	Type       int
	ID         int
	SetID      int
	Sprite     string
	Hair       string
	Foot       string
	HideHair   bool
	HideHat    bool
	VisibleLeg bool
	Label      string
	Price      float64
	Bought     bool
	IsDefault  bool
	IsBonus    bool
	IsWearing  bool
}

type ItemDB struct {
	FileName string
}

//GetJSONString возвращает всю информацию об игроке в виде json-строки
func (i *Item) GetJSONString() string {
	b, _ := json.Marshal(i)
	return string(b)
}

//GetXMLString возвращает всю информацию об игроке в виде xml-строки
func (i *Item) GetXMLString() string {
	b, _ := xml.Marshal(i)
	return string(b)
}

func (i *ItemDB) FindByID(id int) (*Item, bool) {
	s := i.openDB()
	num, err := findNumByID(s, id)
	if err != nil {
		return nil, false
	}
	item := s[num]
	return &item, true
}

func (i *ItemDB) Insert(item Item) error {
	s := i.openDB()
	s = append(s, item)
	return i.saveDB(s)
}

func (i *ItemDB) Update(item Item) error {
	s := i.openDB()
	num, err := findNumByID(s, item.ID)
	if err != nil {
		return err
	}
	s[num] = item
	return i.saveDB(s)
}

func (i *ItemDB) openDB() []Item {
	fileName := "items.json"
	b, err := ioutil.ReadFile(fileName)
	// var b []byte
	var s []Item
	if err != nil {
		_, _ = os.Create(fileName)
		// s := Session{}
		items := make([]Item, 0)
		s = items
		return s
	}
	_ = json.Unmarshal(b, &s)
	return s
}

func (i *ItemDB) saveDB(s []Item) error {
	b, _ := json.Marshal(s)
	return ioutil.WriteFile("items.json", b, 0777)
}

func findNumByID(s []Item, id int) (int, error) {
	for i, v := range s {
		if id == v.ID {
			return i, nil
		}
	}
	return 0, errors.New("not found record by this id")
}
