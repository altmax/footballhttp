package models

import (
	"database/sql"
	"encoding/json"
	"encoding/xml"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

//Gamer объект игрока
type Gamer struct {
	Exp           int    `json:"exp,omitempty"`
	Money         int    `json:"money,omitempty"`
	Hard          int    `json:"hard,omitempty"`
	Wins          int    `json:"wins,omitempty"`
	ID            int64  `json:"id,omitempty"`
	Name          string `json:"name,omitempty"`
	Bonuses       []Bonus
	Inventory     Inventory
	Loses         int `json:"loses,omitempty"`
	Misses        int `json:"misses,omitempty"`
	Goals         int `json:"goals,omitempty"`
	Friends       []string
	Notifications []Notification
	UniqueWord    string `json:"unique_word,omitempty"`
}

//GetJSONString возвращает всю информацию об игроке в виде json-строки
func (g *Gamer) GetJSONString() string {
	b, _ := json.Marshal(g)
	return string(b)
}

//GetXMLString возвращает всю информацию об игроке в виде xml-строки
func (g *Gamer) GetXMLString() string {
	b, _ := xml.Marshal(g)
	return string(b)
}

type GamerTable struct {
	DB interface {
		sqlx.Execer
		Get(interface{}, string, ...interface{}) error
		Select(interface{}, string, ...interface{}) error
	}
}

// FindByID searches gamer record by id
func (table *GamerTable) FindByID(id int64) (*Gamer, error) {
	query := `select id, exp, money, hardmoney, wins, loses, misses, name, goals, unique_word 
              from gamer
             where id = ?;`

	var gamer Gamer

	err := table.DB.Get(&gamer, query, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}

	return &gamer, nil
}

// FindByUniqueWord searches gamer record by mnemocode
func (table *GamerTable) FindByUniqueWord(uniqueWord string) (*Gamer, error) {
	query := `select id, exp, money, hardmoney, wins, loses, misses, name, goals, unique_word 
              from gamer
             where unique_word = ?;`

	var gamer Gamer

	err := table.DB.Get(&gamer, query, uniqueWord)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}

	return &gamer, nil
}

// FindAll returns all gamers
func (table *GamerTable) FindAll() ([]Gamer, error) {
	query := `select id, exp, money, hardmoney, wins, loses, misses, name, goals, unique_word
              from gamer;`

	var gamers []Gamer

	err := table.DB.Select(&gamers, query)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "select from db failed")
	}

	return gamers, nil
}

// Insert inserts record to db and sets created ID to gamer
func (table *GamerTable) Insert(gamer Gamer) (int64, error) {
	query := `insert into gamer (exp, money, hardmoney, wins, loses, misses, name, goals, unique_word) values (?, ?, ?, ?, ?, ?, ?, ?, ?);`

	res, err := table.DB.Exec(query, gamer.Exp, gamer.Money, gamer.Hard, gamer.Wins, gamer.Loses, gamer.Misses, gamer.Name, gamer.Goals, gamer.UniqueWord)
	if err != nil {
		return 0, errors.Wrap(err, "exec failed")
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, errors.Wrap(err, "get last insert id failed")
	}

	return id, nil
}

// Exist check if record present in db
func (table *GamerTable) Exist(id int64) (bool, error) {
	query := `select id from gamer where id = ?;`

	var i int64

	err := table.DB.Get(&i, query, id)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, errors.Wrap(err, "get from db failed")
	}

	return true, nil
}

// Update updates record in db by ID
func (table *GamerTable) Update(gamer Gamer) error {
	exist, err := table.Exist(gamer.ID)
	if err != nil {
		return errors.Wrap(err, "check for exist failed")
	}
	if !exist {
		return errors.New("record not found")
	}

	query := `update gamer exp = ?, money = ?, hardmoney = ?, wins = ?, loses = ?, misses = ?, name = ?, goals = ? where id = ?;`

	_, err = table.DB.Exec(query, gamer.Exp, gamer.Money, gamer.Hard, gamer.Wins, gamer.Loses, gamer.Misses, gamer.Name, gamer.Goals, gamer.UniqueWord, gamer.ID)
	if err != nil {
		return errors.Wrap(err, "exec failed")
	}

	return nil
}
