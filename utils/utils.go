package utils

import "net/http"

//CheckFormField проверяет на пустоту требуемуе поля в request. Возвращает true и имя поля, если его нет или пустое. Вернет false, если поле непустое
func CheckFormField(r *http.Request, fields ...string) (bool, string) {
	for _, v := range fields {
		if r.FormValue(v) == "" {
			return true, v
		}
	}
	return false, ""
}

//GetFieldsFromForm возвращает требуемые поля из request. Если поля пусты(не переданы) вернет nil
func GetFieldsFromForm(r *http.Request, fields ...string) map[string]string {
	if ok, _ := CheckFormField(r, fields...); ok {
		return nil
	}
	data := make(map[string]string)
	for _, v := range fields {
		data[v] = r.FormValue(v)
	}
	return data
}
