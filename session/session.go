package session

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"bitbucket.org/altmax/footballhttp/models"
)

const fileName = "db.json"

type Session struct {
	Gamers map[string]*models.Gamer
}

//NewSession инициация сессии и подгрузка данных из локального файла db.json(будет создан, если не существует)
func NewSession() *Session {
	b, err := ioutil.ReadFile(fileName)
	// var b []byte
	var s Session
	if err != nil {
		_, _ = os.Create(fileName)
		s := Session{}
		gamers := make(map[string]*models.Gamer)
		s.Gamers = gamers
		return &s
	}
	_ = json.Unmarshal(b, &s)
	return &s
}

//Add добавляет нового игрока в сессию и сохраняет в файл
func (s *Session) Add(id, name string) *models.Gamer {
	gamer := models.Gamer{ID: id, Exp: 0, Money: 0, Hard: 0, Wins: 0, Name: name}
	s.Gamers[id] = &gamer
	b, _ := json.Marshal(s)
	ioutil.WriteFile(fileName, b, 0777)
	return &gamer
}

//GetGamer возвращает игрока и статус о его наличии
func (s *Session) GetGamer(id string) (*models.Gamer, bool) {
	gamer, ok := s.Gamers[id]
	return gamer, ok
}

//ViewAll выводит на консоль всех игроков
func (s *Session) ViewAll() {
	k := 1
	for _, v := range s.Gamers {
		fmt.Println(k, ") ", v)
		k++
	}
}
