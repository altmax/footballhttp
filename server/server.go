package server

import (
	"bufio"
	"errors"
	"net/http"
	"os"

	"bitbucket.org/altmax/footballhttp/middleware"
	"bitbucket.org/altmax/footballhttp/routes"

	"strconv"

	"net/url"

	"io/ioutil"

	"strings"

	"github.com/go-martini/martini"
)

var (
	session *Session
)

//sendMessage отправляет пост запрос на мастер сервер и возвращает ответ сервера
func sendMessage(path string, fields map[string]string) (string, bool) {
	urlPath := "http://127.0.0.1:10001" + path
	form := url.Values{}
	// log.Println(fields)
	for i, v := range fields {
		// log.Println("sendMessage > ", i, " : ", v)
		form.Add(i, v)
		form.Set(i, v)
	}
	req, err := http.NewRequest("POST", urlPath, strings.NewReader(form.Encode()))
	if err != nil {
		return err.Error(), false
	}
	// req.PostForm = form
	// log.Println(req)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("football", "HTTP")
	hc := http.Client{}
	resp, err := hc.Do(req)
	if err != nil {
		return err.Error(), false
	}
	if err == nil {
		body, _ := ioutil.ReadAll(resp.Body)
		return string(body), true
	}
	return errors.New("master bad connection").Error(), false
}

//scanner читает из консоли
func scanner() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		switch scanner.Text() {
		case "exit":
			os.Exit(0)
		case "getAllUsers":
			session.ViewAll()
		}
	}
}

//Start запуск http-сервера на указанном порте
func Start(port int) {
	session = NewSession()
	m := martini.Classic()
	defer m.RunOnAddr(":" + strconv.Itoa(port))

	// m.Handlers(CheckUniqKey)
	m.Any("/connect", middleware.CheckUniqKey, routes.Connection)
	m.Any("/matchResults", middleware.ValidAPI, routes.Update)
	m.Any("/matchmaking", routes.MatchMaking)
	// m.Use(CheckUniqKey)
	// m.Use(CheckUpdate)
	// m.Use(CheckUniqKey)
}
